const path = require('path')
const { spawn } = require('child_process')

class ItermTerminalAdapter {
  constructor(workspaceRoot = null) {
    this.itermScriptPath = path.join(__dirname, 'open_in_iterm.js')
    this.workspaceRoot = workspaceRoot
  }

  run(command, withShow = true) {
    if(this.workspaceRoot) command = `cd ${this.workspaceRoot.fsPath} && ${command}`

    let scriptArguments = command.split(' ')
    scriptArguments.unshift(this.itermScriptPath)
    scriptArguments.unshift('JavaScript')
    scriptArguments.unshift('-l')
    spawn("osascript", scriptArguments)
  }

  iterm() {
    return true
  }

  vscode() {
    return false
  }
}

module.exports = {
  ItermTerminalAdapter
}
